function love.load(arg)
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
end

function love.load()
  image = love.graphics.newImage('red-triangle.png')
end

function love.draw()
  love.graphics.setBackgroundColor(80, 0, 0)
  love.graphics.draw(image,
    love.graphics.getWidth()/2 - image:getWidth()/2,
    love.graphics.getHeight()/2 - image:getHeight()/2)
end

function love.keypressed(key, scancode, isrepeat)
  if key == "f" and not isrepeat then
    love.window.setFullscreen(not love.window.getFullscreen())
  end
end
