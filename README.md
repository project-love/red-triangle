# Red Triangle

This is a minimally functional LÖVE "game" that exists only to test my development workflow and tools.
See a live demo at https://redtriangle.jdmarble.pw .

Downloads:

  * [red-triangle.love](http://jdmarble.gitlab.io/red-triangle/red-triangle.love)
  * [Windows executable (32-bit)](http://jdmarble.gitlab.io/red-triangle/Red Triangle-win32.zip)
  * [Windows executable (64-bit)](http://jdmarble.gitlab.io/red-triangle/Red Triangle-win64.zip)
  * [Mac OSX](http://jdmarble.gitlab.io/red-triangle/Red Triangle-macosx.zip)

## Building

To avoid the nuisance of configuring the build environment, you should use Docker:

    docker run --rm --tty --interactive \
               --volume=`pwd`:/workspace:Z --workdir=/workspace --env="HOME=/workspace" \
               --user=`id --user`:`id --group` \
               registry.gitlab.com/jdmarble/project-love-docker:1.2.1 ./build.sh
