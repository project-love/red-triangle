#!/bin/sh
set -x

VERSION=${1:-0}
PROJECTROOT=$(pwd)
SOURCEDIR=${PROJECTROOT}/sources
RELEASEDIR=${PROJECTROOT}/releases
PUBLICDIR=${PROJECTROOT}/public

mkdir $SOURCEDIR
cp *.lua $SOURCEDIR
cp *.png $SOURCEDIR

# Generate .love file and stand-alone packages for various platforms
love-release -v $VERSION -W 32 -W 64 -M -D $RELEASEDIR $SOURCEDIR

# Generate a live web demo using love.js
cd /love.js/release-compatibility
# First invocation is for generating the .emscripten config in $HOME
python2.7 ../emscripten/tools/file_packager.py --help
mkdir $PUBLICDIR
python2.7 ../emscripten/tools/file_packager.py ${PUBLICDIR}/game.data --preload ${SOURCEDIR}@/ --js-output=${PUBLICDIR}/game.js
cp -r * $PUBLICDIR

